var serverApps = 
{
    "playground" : [
        {
            "name" : "Component Test",
            "url":  "./apps/playground/wndComponent.html",
            "color": "#3498DB"
        }
    ],
    "powerreport" : [
        {
            "name" : "PowerReport",
            "url":  "./apps/powerReport/index.html",
            "color": "#9B59B6"
        }
    ]
}

